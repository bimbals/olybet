<?php

declare(strict_types=1);

namespace App\Api;

use Doctrine\Common\Collections\ArrayCollection;

interface ApiProblemAwareInterface
{
    /** @return ApiProblem[] * */
    public function getErrors(): ArrayCollection;

    public function addError(ApiProblem $apiProblem);
}
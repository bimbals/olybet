<?php

declare(strict_types=1);

namespace App\Api;

use App\Serializer\SerializerHelper;
use Symfony\Component\HttpFoundation\Response;

class ResponseFactory
{
    public function createResponse(ApiProblemAwareInterface $apiProblemContainer, int $statusCode)
    {
        $json = SerializerHelper::serializeJson($apiProblemContainer);

        $response = new Response(
            $json,
            $statusCode
        );

        $response->headers->set('Content-Type', 'application/problem+json');

        return $response;
    }
}
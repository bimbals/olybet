<?php

declare(strict_types=1);

namespace App\Api;

class ApiProblem
{
    const TYPE_VALIDATION_ERROR = 'validation_error';
    const TYPE_UNKNOWN_ERROR = 0;
    const TYPE_INVALID_REQUEST_BODY_FORMAT = 1;
    const TYPE_MIN_STAKE_AMOUNT = 2;
    const TYPE_MAX_STAKE_AMOUNT = 3;
    const TYPE_MIN_SELECTIONS = 4;
    const TYPE_MAX_SELECTIONS = 5;
    const TYPE_MIN_ODDS = 6;
    const TYPE_MAX_ODSS = 7;
    const TYPE_DUPLICATE_SELECTION = 8;
    const TYPE_MAX_WIN_AMOUNT = 9;
    const TYPE_PROCESSING = 10;
    const TYPE_INSUFFICIENT_BALANCE = 11;

    private static $titles = array(
        self::TYPE_VALIDATION_ERROR => 'There was a validation error',
        self::TYPE_UNKNOWN_ERROR => 'Unknown error',
        self::TYPE_INVALID_REQUEST_BODY_FORMAT => 'Betslip structure mismatch',
        self::TYPE_MIN_STAKE_AMOUNT => 'Minimum stake amount is %s',
        self::TYPE_MAX_STAKE_AMOUNT => 'Maximum stake amount is %s',
        self::TYPE_MIN_SELECTIONS => 'Minimum number of selections is %s',
        self::TYPE_MAX_SELECTIONS => 'Maximum number of selections is %s',
        self::TYPE_MIN_ODDS => 'Minimum odds are %s',
        self::TYPE_MAX_ODSS => 'Maximum odds are %s',
        self::TYPE_DUPLICATE_SELECTION => 'Duplicate selection found',
        self::TYPE_MAX_WIN_AMOUNT => 'Maximum win amount is %s',
        self::TYPE_PROCESSING => 'Your previous action is not finished yet',
        self::TYPE_INSUFFICIENT_BALANCE => 'Insufficient balance',
    );

    private $type;
    private $title;
    private $extraData = array();

    /**
     * @param mixed|null $parameter
     */
    public function __construct(int $type = null, $parameter = null)
    {
        $this->type = $type;

        if (null === $type) {
            $this->type = self::TYPE_UNKNOWN_ERROR;
            $this->title = self::$titles[self::TYPE_UNKNOWN_ERROR];
        } else {
            if (!isset(self::$titles[$type])) {
                throw new \Exception(sprintf(
                    'No title for type "%s". Did you made it up?',
                    $type
                ));
            }

            $this->title = sprintf(self::$titles[$type], $parameter);
        }
    }

    public function toArray(): array
    {
        return array_merge(
            $this->extraData,
            array(
                'code' => $this->type,
                'message' => $this->title,
            )
        );
    }

    public function set(string $name, $value): self
    {
        $this->extraData[$name] = $value;

        return $this;
    }

    /*
     * For serialization
     */
    public function getMessage(): string
    {
        return $this->title;
    }

    /*
     * For serialization
     */
    public function getCode(): int
    {
        return $this->type;
    }
}

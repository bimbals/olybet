<?php

declare(strict_types=1);

namespace App\Api;

use Doctrine\Common\Collections\ArrayCollection;

class ApiProblemNullContainer implements ApiProblemAwareInterface
{
    private $errors;

    public function __construct()
    {
        $this->errors = new ArrayCollection();
    }

    public function addError(ApiProblem $apiProblem): self
    {
        $this->errors->add($apiProblem);

        return $this;
    }

    public function getErrors(): ArrayCollection
    {
        return $this->errors;
    }
}
<?php

declare(strict_types=1);

namespace App\Api;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ApiProblemException extends HttpException
{
    private $apiProblemContainer;

    public function __construct(
        ApiProblemAwareInterface $apiProblemContainer,
        int $statusCode,
        \Exception $previous = null,
        array $headers = array(),
        int $code = 0
    ) {
        $this->apiProblemContainer = $apiProblemContainer;
        $message = isset(Response::$statusTexts[$statusCode])
            ? Response::$statusTexts[$statusCode]
            : 'Unknown status code';

        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    public function getApiProblemContainer(): ApiProblemAwareInterface
    {
        return $this->apiProblemContainer;
    }
}

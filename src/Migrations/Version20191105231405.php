<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191105231405 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE balance_transaction (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, player_id INTEGER NOT NULL, amount DOUBLE PRECISION NOT NULL, amount_before DOUBLE PRECISION NOT NULL)');
        $this->addSql('CREATE INDEX IDX_A70FE73399E6F5DF ON balance_transaction (player_id)');
        $this->addSql('CREATE TABLE bet (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, stake_amount DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL)');
        $this->addSql('CREATE TABLE bet_selections (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bet_id INTEGER NOT NULL, selection_id INTEGER NOT NULL, odds DOUBLE PRECISION NOT NULL)');
        $this->addSql('CREATE INDEX IDX_837B8D61D871DC26 ON bet_selections (bet_id)');
        $this->addSql('CREATE INDEX IDX_837B8D61E48EFE78 ON bet_selections (selection_id)');
        $this->addSql('CREATE TABLE player (id INTEGER NOT NULL, balance DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE selection (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE balance_transaction');
        $this->addSql('DROP TABLE bet');
        $this->addSql('DROP TABLE bet_selections');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE selection');
    }
}

<?php

declare(strict_types=1);

namespace App\Form;

use App\DTO\BetslipDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BetslipDTOType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('playerId', IntegerType::class)
            ->add('stakeAmount', NumberType::class, [
                'scale' => 2,
            ])
            ->add('errors', null, [
                'mapped' => false
            ])
            ->add('selections', CollectionType::class, [
                'entry_type' => SelectionDTOType::class,
                'allow_add' => true,
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BetslipDTO::class,
        ]);
    }
}

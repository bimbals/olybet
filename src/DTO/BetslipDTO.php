<?php

declare(strict_types=1);

namespace App\DTO;

use App\Api\ApiProblem;
use App\Api\ApiProblemAwareInterface;
use App\Validator\Betslip;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DTO - data transfer object.
 *
 * @Betslip()
 */
class BetslipDTO implements ApiProblemAwareInterface
{
    private $playerId;
    private $stakeAmount;
    private $errors;
    private $selections;

    public function __construct()
    {
        $this->errors = new ArrayCollection();
        $this->selections = new ArrayCollection();
    }

    public function getPlayerId(): ?int
    {
        return $this->playerId;
    }

    public function setPlayerId(int $playerId): self
    {
        $this->playerId = $playerId;

        return $this;
    }

    public function getStakeAmount(): ?float
    {
        return $this->stakeAmount;
    }

    public function setStakeAmount(float $stakeAmount): self
    {
        $this->stakeAmount = $stakeAmount;

        return $this;
    }

    public function getErrors(): ArrayCollection
    {
        return $this->errors;
    }

    public function addError(ApiProblem $apiProblem): self
    {
        $exists = $this->errors->exists(
            function ($key, $element) use ($apiProblem) {
                /** @var ApiProblem $element */
                return $apiProblem->getCode() === $element->getCode();
            }
        );

        if (!$exists) {
            $this->errors->add($apiProblem);
        }

        return $this;
    }

    public function getSelections(): ArrayCollection
    {
        return $this->selections;
    }

    public function addSelection(SelectionDTO $selection): self
    {
        $this->selections->add($selection);

        return $this;
    }

    public function removeSelection(SelectionDTO $selection): self
    {
        $this->selections->removeElement($selection);

        return $this;
    }
}
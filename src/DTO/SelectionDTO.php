<?php

declare(strict_types=1);

namespace App\DTO;

use App\Api\ApiProblem;
use App\Api\ApiProblemAwareInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DTO - data transfer object.
 */
class SelectionDTO implements ApiProblemAwareInterface
{
    private $id;
    private $odds;
    private $errors;

    public function __construct()
    {
        $this->errors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $Id): self
    {
        $this->id = $Id;

        return $this;
    }

    public function getOdds(): ?float
    {
        return $this->odds;
    }

    public function setOdds(float $odds): self
    {
        $this->odds = $odds;

        return $this;
    }

    public function getErrors(): ArrayCollection
    {
        return $this->errors;
    }

    public function addError(ApiProblem $apiProblem): self
    {
        $exists = $this->errors->exists(
            function ($key, $element) use ($apiProblem) {
                /** @var ApiProblem $element */
                return $apiProblem->getCode() === $element->getCode();
            }
        );

        if (!$exists) {
            $this->errors->add($apiProblem);
        }

        return $this;
    }
}
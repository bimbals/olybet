<?php

namespace App\Repository;

use App\Entity\BetSelections;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BetSelections|null find($id, $lockMode = null, $lockVersion = null)
 * @method BetSelections|null findOneBy(array $criteria, array $orderBy = null)
 * @method BetSelections[]    findAll()
 * @method BetSelections[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BetSelectionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BetSelections::class);
    }

    // /**
    //  * @return BetSelections[] Returns an array of BetSelections objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BetSelections
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

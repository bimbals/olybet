<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BalanceTransaction", mappedBy="player", orphanRemoval=true)
     */
    private $balanceTransactions;

    public function __construct()
    {
        $this->balanceTransactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getBalance(): ?float
    {
        return $this->balance;
    }

    public function setBalance(float $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return Collection|BalanceTransaction[]
     */
    public function getBalanceTransactions(): Collection
    {
        return $this->balanceTransactions;
    }

    public function addBalanceTransaction(BalanceTransaction $balanceTransaction): self
    {
        if (!$this->balanceTransactions->contains($balanceTransaction)) {
            $this->balanceTransactions[] = $balanceTransaction;
            $balanceTransaction->setPlayer($this);
        }

        return $this;
    }

    public function removeBalanceTransaction(BalanceTransaction $balanceTransaction): self
    {
        if ($this->balanceTransactions->contains($balanceTransaction)) {
            $this->balanceTransactions->removeElement($balanceTransaction);
            // set the owning side to null (unless already changed)
            if ($balanceTransaction->getPlayer() === $this) {
                $balanceTransaction->setPlayer(null);
            }
        }

        return $this;
    }
}

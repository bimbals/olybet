<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BetSelectionsRepository")
 */
class BetSelections
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bet", inversedBy="betSelections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bet;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Selection", inversedBy="betSelections")
     * @ORM\JoinColumn(nullable=false)
     */
    private $selection;

    /**
     * @ORM\Column(type="float")
     */
    private $odds;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBet(): ?Bet
    {
        return $this->bet;
    }

    public function setBet(?Bet $bet): self
    {
        $this->bet = $bet;

        return $this;
    }

    public function getSelection(): ?Selection
    {
        return $this->selection;
    }

    public function setSelection(?Selection $selection): self
    {
        $this->selection = $selection;

        return $this;
    }

    public function getOdds(): ?float
    {
        return $this->odds;
    }

    public function setOdds(float $odds): self
    {
        $this->odds = $odds;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SelectionRepository")
 */
class Selection
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BetSelections", mappedBy="selection", orphanRemoval=true)
     */
    private $betSelections;

    public function __construct()
    {
        $this->betSelections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }


    /**
     * @return Collection|BetSelections[]
     */
    public function getBetSelections(): Collection
    {
        return $this->betSelections;
    }

    public function addBetSelection(BetSelections $betSelection): self
    {
        if (!$this->betSelections->contains($betSelection)) {
            $this->betSelections[] = $betSelection;
            $betSelection->setSelection($this);
        }

        return $this;
    }

    public function removeBetSelection(BetSelections $betSelection): self
    {
        if ($this->betSelections->contains($betSelection)) {
            $this->betSelections->removeElement($betSelection);
            // set the owning side to null (unless already changed)
            if ($betSelection->getSelection() === $this) {
                $betSelection->setSelection(null);
            }
        }

        return $this;
    }
}

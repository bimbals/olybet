<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BetRepository")
 */
class Bet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $stake_amount;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BetSelections", mappedBy="bet", orphanRemoval=true)
     */
    private $betSelections;

    public function __construct()
    {
        $this->betSelections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStakeAmount(): ?float
    {
        return $this->stake_amount;
    }

    public function setStakeAmount(float $stake_amount): self
    {
        $this->stake_amount = $stake_amount;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|BetSelections[]
     */
    public function getBetSelections(): Collection
    {
        return $this->betSelections;
    }

    public function addBetSelection(BetSelections $betSelection): self
    {
        if (!$this->betSelections->contains($betSelection)) {
            $this->betSelections[] = $betSelection;
            $betSelection->setBet($this);
        }

        return $this;
    }

    public function removeBetSelection(BetSelections $betSelection): self
    {
        if ($this->betSelections->contains($betSelection)) {
            $this->betSelections->removeElement($betSelection);
            // set the owning side to null (unless already changed)
            if ($betSelection->getBet() === $this) {
                $betSelection->setBet(null);
            }
        }

        return $this;
    }
}

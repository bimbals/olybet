<?php

declare(strict_types=1);

namespace App\Validator;

use App\Api\ApiProblem;
use App\DTO\BetslipDTO;
use App\DTO\SelectionDTO;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BetslipValidator extends ConstraintValidator
{
    const MIN_STAKE_AMOUNT = 0.3;
    const MAX_STAKE_AMOUNT = 10000;

    const MIN_SELECTIONS_COUNT = 1;
    const MAX_SELECTIONS_COUNT = 20;

    const MIN_ODDS_AMOUNT = 1;
    const MAX_ODDS_AMOUNT = 10000;

    const MAX_WIN_AMOUNT = 20000;

    /** @var ValidatorInterface $validator */
    private $validator;

    /** @var BetslipDTO $betslipModel */
    private $betslipDTO;

    // TODO: file is too big, implement decorator or other pattern for better readability
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint Betslip */

        if (!$constraint instanceof Betslip) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\Betslip');
        }

        if (null === $value || '' === $value) {
            return;
        }

        $this->validator = $this->context->getValidator();
        $this->betslipDTO = $value;

        $this->validateTypes();
        $this->validateStructure();

        // Stop validation if there are some type or structure validation errors
        if ($this->hasErrors()) {
            $this->buildMainFormError($constraint);
            return;
        }

        $this->validateStakeAmount();
        $this->validateSelectionsCount();
        $this->validateSelectionDuplicates();
        $this->validateWinAmount();
        // TODO: Separate SelectionDTO Validator and BetslipDTO Validator into different files
        $this->validateSelectionOdds();

        if ($this->hasErrors()) {
            $this->buildMainFormError($constraint);
        }
    }

    protected function validateTypes()
    {
        $violations = null;

        $playerIdViolations = $this->validator->validate(
            $this->betslipDTO->getPlayerId(),
            [new Assert\Type(['type' => 'integer'])]
        );

        $stakeAmountViolations = $this->validator->validate(
            $this->betslipDTO->getStakeAmount(),
            [new Assert\Type(['type' => 'float'])]
        );

        if ($playerIdViolations->count() > 0 || $stakeAmountViolations->count() > 0) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT));
        }
    }

    protected function validateStructure()
    {
        if (!$this->betslipDTO->getPlayerId() || !$this->betslipDTO->getStakeAmount() || !$this->betslipDTO->getSelections()) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT));
        }
    }

    protected function hasErrors()
    {
        if ($this->betslipDTO->getErrors()->count() > 0) {
            return true;
        }

        foreach ($this->betslipDTO->getSelections() as $selection) {
            /** @var SelectionDTO $selection */
            if ($selection->getErrors()->count() > 0) {
                return true;
            }
        }
    }

    private function buildMainFormError(Constraint $constraint)
    {
        $this->context->buildViolation($constraint->message)
            ->addViolation();
    }

    protected function validateStakeAmount()
    {
        $violations = $this->validator->validate($this->betslipDTO->getStakeAmount(), [
            new Assert\GreaterThanOrEqual(['value' => self::MIN_STAKE_AMOUNT]),
        ]);

        if ($violations->count() > 0) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_MIN_STAKE_AMOUNT, self::MIN_STAKE_AMOUNT));
        }

        $violations = $this->validator->validate($this->betslipDTO->getStakeAmount(), [
            new Assert\LessThanOrEqual(['value' => self::MAX_STAKE_AMOUNT]),
        ]);

        if ($violations->count() > 0) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_MAX_STAKE_AMOUNT, self::MAX_STAKE_AMOUNT));
        }
    }

    protected function validateSelectionsCount()
    {
        $selectionsCount = $this->betslipDTO->getSelections()->count();

        $violations = $this->validator->validate($selectionsCount, [
            new Assert\GreaterThanOrEqual(['value' => self::MIN_SELECTIONS_COUNT]),
        ]);

        if ($violations->count() > 0) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_MIN_SELECTIONS, self::MIN_SELECTIONS_COUNT));
        }

        $violations = $this->validator->validate($selectionsCount, [
            new Assert\LessThanOrEqual(['value' => self::MAX_SELECTIONS_COUNT]),
        ]);

        if ($violations->count() > 0) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_MAX_SELECTIONS, self::MAX_SELECTIONS_COUNT));
        }
    }

    private function validateSelectionDuplicates()
    {
        $tmpSelectionsArray = [];
        foreach ($this->betslipDTO->getSelections() as $selection) {
            $tmpSelectionsArray[$selection->getId()][] = $selection;
        }

        foreach ($tmpSelectionsArray as $tmpSelections) {
            if (count($tmpSelections) <= 1) {
                continue;
            }

            foreach ($tmpSelections as $tmpSelection) {
                /** @var SelectionDTO $tmpSelection */
                $tmpSelection
                    ->addError(new ApiProblem(ApiProblem::TYPE_DUPLICATE_SELECTION));
            }
        }
    }

    protected function validateWinAmount()
    {
        $oddsSum = 1;

        foreach ($this->betslipDTO->getSelections() as $selection) {
            /** @var SelectionDTO $selection */
            $oddsSum *= $selection->getOdds();
        }

        $winAmount = $this->betslipDTO->getStakeAmount() * $oddsSum;

        if ($winAmount > self::MAX_WIN_AMOUNT) {
            $this->betslipDTO
                ->addError(new ApiProblem(ApiProblem::TYPE_MAX_WIN_AMOUNT, self::MAX_WIN_AMOUNT));
        }
    }

    protected function validateSelectionOdds()
    {
        foreach ($this->betslipDTO->getSelections() as $selection) {
            /** @var SelectionDTO $selection */
            $odds = $selection->getOdds();

            $violations = $this->validator->validate($odds, [
                new Assert\GreaterThanOrEqual(['value' => self::MIN_ODDS_AMOUNT]),
            ]);

            if ($violations->count() > 0) {
                $selection
                    ->addError(new ApiProblem(ApiProblem::TYPE_MIN_ODDS, self::MIN_ODDS_AMOUNT));
            }

            $violations = $this->validator->validate($odds, [
                new Assert\LessThanOrEqual(['value' => self::MAX_ODDS_AMOUNT]),
            ]);

            if ($violations->count() > 0) {
                $selection
                    ->addError(new ApiProblem(ApiProblem::TYPE_MAX_ODSS, self::MAX_ODDS_AMOUNT));
            }
        }
    }
}

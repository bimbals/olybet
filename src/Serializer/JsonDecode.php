<?php

declare(strict_types=1);

namespace App\Serializer;

use Symfony\Component\Serializer\Encoder\JsonDecode as BaseJsonDecode;

class JsonDecode extends BaseJsonDecode
{
    public function decode($data, $format, array $context = [])
    {
        $context[self::ASSOCIATIVE] = true;
        $decodedData = parent::decode($data, $format, $context);

        $result = [];
        foreach ($decodedData as $propertyName => $value) {
            $newPropertyName = $this->convertSnakeCaseToCamelCase($propertyName);
            $result[$newPropertyName] = $value;
        }

        return $result;
    }

    private function convertSnakeCaseToCamelCase($propertyName)
    {
        $camelCasedName = preg_replace_callback('/(^|_|\.)+(.)/', function ($match) {
            return ('.' === $match[1] ? '_' : '') . strtoupper($match[2]);
        }, $propertyName);

        return lcfirst($camelCasedName);
    }
}

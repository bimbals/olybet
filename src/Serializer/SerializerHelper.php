<?php

declare(strict_types=1);

namespace App\Serializer;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SerializerHelper
{
    static public function serializeJson($data, $groups = []): string
    {
        $groups = $groups ?: ['default'];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizers = array(new ObjectNormalizer($classMetadataFactory, new CamelCaseToSnakeCaseNameConverter()));
        $encoders = array(
            new JsonEncoder(
                new JsonEncode(JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
            ),
        );
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->serialize($data, 'json');
    }

    static public function decodeJson($data)
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizers = array(new ObjectNormalizer($classMetadataFactory));
        $encoders = array(
            new JsonEncoder(
                null,
                new JsonDecode()
            ),
        );
        $serializer = new Serializer($normalizers, $encoders);

        return $serializer->decode($data, 'json');
    }
}
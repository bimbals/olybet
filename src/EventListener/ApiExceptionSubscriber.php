<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use App\Api\ApiProblemNullContainer;
use App\Api\ResponseFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    private $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::EXCEPTION => 'onKernelException'
        );
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $e = $event->getException();

        $statusCode = $e instanceof HttpExceptionInterface ? $e->getStatusCode() : 500;

        // allow 500 errors to be thrown
        if ($statusCode >= 500) {
            return;
        }

        if ($e instanceof ApiProblemException) {
            $apiProblemContainer = $e->getApiProblemContainer();
        } else {
            $apiProblemContainer = new ApiProblemNullContainer();
            $apiProblemContainer->addError(new ApiProblem(ApiProblem::TYPE_UNKNOWN_ERROR));
        }

        $response = $this->responseFactory->createResponse($apiProblemContainer, $e->getStatusCode());

        $event->setResponse($response);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Api\ApiProblem;
use App\Api\ApiProblemAwareInterface;
use App\Api\ApiProblemException;
use App\Api\ApiProblemNullContainer;
use App\Serializer\SerializerHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

class BaseController extends AbstractController
{
    protected function createApiResponse($data, int $statusCode = 200, array $groups = null): Response
    {
        $json = SerializerHelper::serializeJson($data, $groups);

        return new Response(
            $json, $statusCode, [
                'Content-Type' => 'application/json; charset=utf-8',
            ]
        );
    }

    protected function processForm(Request $request, FormInterface $form)
    {
        try {
            $data = SerializerHelper::decodeJson($request->getContent());
        } catch (NotEncodableValueException $e) {
            $apiProblemNullContainer = new ApiProblemNullContainer();
            $apiProblemNullContainer->addError(new ApiProblem(ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT));
            throw new ApiProblemException($apiProblemNullContainer, 400);
        }

        $clearMissing = $request->getMethod() != 'PATCH';
        $form->submit($data, $clearMissing);
    }

    protected function throwApiProblemValidationException(FormInterface $form)
    {
        /** @var ApiProblemAwareInterface $DTO */
        $DTO = $form->getData();

        if ($form->getExtraData()) {
            $DTO->addError(new ApiProblem(ApiProblem::TYPE_INVALID_REQUEST_BODY_FORMAT));
        }

        throw new ApiProblemException($DTO, 400);
    }
}
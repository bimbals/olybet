<?php

declare(strict_types=1);

namespace App\Controller;

use App\Api\ApiProblem;
use App\Api\ApiProblemException;
use App\DTO\BetslipDTO;
use App\DTO\SelectionDTO;
use App\Entity\BalanceTransaction;
use App\Entity\Bet;
use App\Entity\BetSelections;
use App\Entity\Player;
use App\Entity\Selection;
use App\Form\BetslipDTOType;
use App\Repository\PlayerRepository;
use App\Repository\SelectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends BaseController
{
    /**
     * @Route("/api/bet", methods={"POST"})
     *
     * TODO: seperate into different files (services, factories)
     */
    public function betAction(
        Request $request,
        EntityManagerInterface $entityManager,
        PlayerRepository $playerRepository,
        SelectionRepository $selectionRepository
    ) {
        $form = $this->createForm(BetslipDTOType::class);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            $this->throwApiProblemValidationException($form);
        }

        /** @var BetslipDTO $betslipDTO */
        $betslipDTO = $form->getData();

        // create Player
        $playerId = $betslipDTO->getPlayerId();
        $player = $playerRepository->find($playerId);
        if (!$player) {
            $player = (new Player())
                ->setId($playerId)
                ->setBalance(1000);
            $entityManager->persist($player);
        }

        // check if balance is sufficient for a bet
        if ($player->getBalance() < $betslipDTO->getStakeAmount()) {
            $betslipDTO->addError(new ApiProblem(ApiProblem::TYPE_INSUFFICIENT_BALANCE));
            throw new ApiProblemException($betslipDTO, 400);
        }

        // set new balance
        $oldBalance = $player->getBalance();
        $newBalance = $player->getBalance() - $betslipDTO->getStakeAmount();
        $player->setBalance($newBalance);
        $entityManager->persist($player);

        // create BalanceTransaction
        $balanceTransaction = (new BalanceTransaction())
            ->setPlayer($player)
            ->setAmountBefore($oldBalance)
            ->setAmount($newBalance);
        $entityManager->persist($balanceTransaction);

        // create Bet
        $bet = (new Bet())
            ->setStakeAmount($betslipDTO->getStakeAmount())
            ->setCreatedAt(new \DateTime()); // TODO: create with annotations
        $entityManager->persist($bet);

        foreach ($betslipDTO->getSelections() as $selectionDTO) {
            /** @var SelectionDTO $selectionDTO */
            // create Selection
            $selectionId = $selectionDTO->getId();
            $selection = $selectionRepository->find($selectionId);
            if (!$selection) {
                $selection = (new Selection())
                    ->setId($selectionId);
                $entityManager->persist($selection);
            }

            // create BetSelections
            $betSelections = (new BetSelections())
                ->setBet($bet)
                ->setSelection($selection)
                ->setOdds($selectionDTO->getOdds());
            $entityManager->persist($betSelections);
        }

        $entityManager->flush();

        return $this->createApiResponse([], 201);
    }
}
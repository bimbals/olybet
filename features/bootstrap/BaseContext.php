<?php

declare(strict_types=1);

use Behat\Behat\Context\Context;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Component\HttpKernel\KernelInterface;

class BaseContext implements Context
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario
     */
    public function clearData()
    {
        //$purger = new ORMPurger($this->getContainer()->get('doctrine')->getManager());
        //$purger->purge();
    }

    /**
     * @BeforeScenario @fixtures
     */
    public function loadFixtures()
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');
        $loader = new ContainerAwareLoader($container);
        $loader->loadFromDirectory(__DIR__.'/../../src/DataFixtures');
        $executor = new ORMExecutor($em);
        $executor->execute($loader->getFixtures(), true);
    }

    public function getContainer()
    {
        return $this->kernel->getContainer();
    }
}
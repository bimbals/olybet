Feature: Bet
  In order to place a bet
  As an API client
  I need to be able to create selections and players with balance

  Scenario: Error response on invalid JSON
    Given I have the payload:
      """
      {
        "player_id": 1
        "stake_amount": "5",
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Betslip structure mismatch"
    And the "errors.0.code" property should equal "1"

  Scenario: Error response on extra fields
    Given I have the payload:
      """
      {
        "player_idas": "test",
        "stake_amount": 0.3,
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Betslip structure mismatch"
    And the "errors.0.code" property should equal "1"


  Scenario: Error response on stake_amount out of min range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 0.2,
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Minimum stake amount is 0.3"
    And the "errors.0.code" property should equal "2"

  Scenario: Error response on stake_amount out of max range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 10000.01,
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Maximum stake amount is 10000"
    And the "errors.0.code" property should equal "3"


  Scenario: Error response on selections duplication
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 10000,
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          },
          {
            "id": 1,
            "odds": "1.602"
          },
          {
            "id": 2,
            "odds": "1.602"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "selections" property should be an array
    And the "selections" property should contain 3 items
    And the "selections.0.errors" property should be an array
    And the "selections.0.errors" property should contain 1 item
    And the "selections.0.errors.0.message" property should equal "Duplicate selection found"
    And the "selections.0.errors.0.code" property should equal "8"
    And the "selections.1.errors" property should be an array
    And the "selections.1.errors" property should contain 1 item
    And the "selections.1.errors.0.message" property should equal "Duplicate selection found"
    And the "selections.1.errors.0.code" property should equal "8"
    And the "selections.2.errors" property should be an array
    And the "selections.2.errors" property should contain 0 items

  Scenario: Error response on selections out of min range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 0.3,
        "selections": [
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Minimum number of selections is 1"
    And the "errors.0.code" property should equal "4"

  Scenario: Error response on selections out of max range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 0.3,
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          },
          {
            "id": 2,
            "odds": "1.601"
          },
          {
            "id": 3,
            "odds": "1.601"
          },
          {
            "id": 4,
            "odds": "1.601"
          },
          {
            "id": 5,
            "odds": "1.601"
          },
          {
            "id": 6,
            "odds": "1.601"
          },
          {
            "id": 7,
            "odds": "1.601"
          },
          {
            "id": 8,
            "odds": "1.601"
          },
          {
            "id": 9,
            "odds": "1.601"
          },
          {
            "id": 10,
            "odds": "1.601"
          },
          {
            "id": 11,
            "odds": "1.601"
          },
          {
            "id": 12,
            "odds": "1.601"
          },
          {
            "id": 13,
            "odds": "1.601"
          },
          {
            "id": 14,
            "odds": "1.601"
          },
          {
            "id": 15,
            "odds": "1.601"
          },
          {
            "id": 16,
            "odds": "1.601"
          },
          {
            "id": 17,
            "odds": "1.601"
          },
          {
            "id": 18,
            "odds": "1.601"
          },
          {
            "id": 19,
            "odds": "1.601"
          },
          {
            "id": 20,
            "odds": "1.601"
          },
          {
            "id": 21,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Maximum number of selections is 20"
    And the "errors.0.code" property should equal "5"

  Scenario: Error response on odds out of min range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 0.3,
        "selections": [
          {
            "id": 21,
            "odds": "0.6"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "selections" property should be an array
    And the "selections" property should contain 1 item
    And the "selections.0.errors" property should be an array
    And the "selections.0.errors" property should contain 1 item
    And the "selections.0.errors.0.message" property should equal "Minimum odds are 1"
    And the "selections.0.errors.0.code" property should equal "6"

  Scenario: Error response on odds out of max range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 0.3,
        "selections": [
          {
            "id": 21,
            "odds": "10000.01"
          },
          {
            "id": 22,
            "odds": "10"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "selections" property should be an array
    And the "selections" property should contain 2 items
    And the "selections.0.errors" property should be an array
    And the "selections.0.errors" property should contain 1 item
    And the "selections.0.errors.0.message" property should equal "Maximum odds are 10000"
    And the "selections.0.errors.0.code" property should equal "7"
    And the "selections.1.errors" property should be an array
    And the "selections.1.errors" property should contain 0 items

  Scenario: Error response on max win amount out of max range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": "50000",
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          },
          {
            "id": 1,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 2 items
    And the "errors.0.message" property should equal "Maximum stake amount is 10000"
    And the "errors.0.code" property should equal "3"
    And the "errors.1.message" property should equal "Maximum win amount is 20000"
    And the "errors.1.code" property should equal "9"
    And the "selections" property should be an array
    And the "selections" property should contain 2 items
    And the "selections.0.errors" property should be an array
    And the "selections.0.errors" property should contain 1 item
    And the "selections.0.errors.0.message" property should equal "Duplicate selection found"
    And the "selections.0.errors.0.code" property should equal "8"
    And the "selections.1.errors" property should be an array
    And the "selections.1.errors" property should contain 1 item
    And the "selections.1.errors.0.message" property should equal "Duplicate selection found"
    And the "selections.1.errors.0.code" property should equal "8"

  Scenario: POST to create a bet
    Given I have the payload:
      """
      {
        "player_id": 5,
        "stake_amount": "5",
        "selections": [
          {
            "id": 1,
            "odds": "1.601"
          }
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 201

  Scenario: Error response on selections out of min range
    Given I have the payload:
      """
      {
        "player_id": 1,
        "stake_amount": 0.3,
        "selections": [
        ]
      }
      """
    When I request "POST /api/bet"
    Then the response status code should be 400
    And the "Content-Type" header should be "application/problem+json"
    And the following properties should exist:
      """
      player_id
      stake_amount
      errors
      selections
      """
    And the "errors" property should be an array
    And the "errors" property should contain 1 item
    And the "errors.0.message" property should equal "Minimum number of selections is 1"
    And the "errors.0.code" property should equal "4"

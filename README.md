## How to start

You’ll need to have docker installed on localhost

1. `cd .docker`
2. `docker-compose up -d`
3. `docker-exec -it bash`
4. `composer install`

NOTES:
- By default data is saved in sqlite
- There is a volume in docker-compose.yaml